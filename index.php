<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC A01</title>
</head>
<body>
	<h1>Full Address</h1>
	<p><?php echo getFullAddress("South Korea", "Jung-gu", "Daejeon", "469 Bomunsangongwon-ro"); ?></p>

	<h1>Letter-Based Grading</h1>
	<p><?php echo getLetterGrade(87); ?></p>
	<p><?php echo getLetterGrade(94); ?></p>
	<p><?php echo getLetterGrade(74); ?></p>
</body>
</html>